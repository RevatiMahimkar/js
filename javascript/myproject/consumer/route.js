'use strict';

const Config = require('./config');

module.exports = (server, options) => [
  {
    method: 'POST',
    path: '/consumers',
    config: Config.create
  }
];
