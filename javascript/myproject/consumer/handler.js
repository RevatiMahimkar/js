'use strict';

module.exports = {
  create: (request, reply) => reply(request.pre.consumer).created()
};
