'use strict';

const Validations = require('./validation');
const Handlers = require('./handler');

module.exports = {
  create: {
    validate: Validations.create,
    pre: [
      { method: 'consumers.create(payload)', assign: 'consumer' },
    ],
    handler: Handlers.create
  },
};
