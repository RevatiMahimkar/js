'use strict';

const Boom = require('boom');
const _ = require('lodash');

module.exports = (server, options) => [
  {
    name: 'consumers.create',
    method: create,
    options: {
      bind: server
    }
  }
];

const create = function create(data, next) {
  return next(null, data);
};
