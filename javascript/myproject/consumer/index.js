'use strict';

const Path = require('path');
const Fs = require('fs');
const _ = require('lodash');

exports.register = (server, options, next) => {

  if (server.settings.app.env !== 'test') {
    server.log(['info', 'bootstrap', 'routing'], 'Mounting routes...');
  }

  server.method(require('./method')(server, options));
  server.route(require('./route')(server, options));

  next();
};

exports.register.attributes = {
  name: 'consumer-plugin'
};
