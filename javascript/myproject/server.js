'use strict';

const Hapi = require('hapi');
const Good = require('good');
var Todo= [];
var user_count= 0;
var Joi = require('joi');
var Blipp= require('blipp');

const server = new Hapi.Server();
server.connection({ port: 3000, host: 'localhost' });

server.route({
  method: 'POST',
  path: '/api/v1/consumers',
  handler: function(request, reply) {
    user_count++;
    request.payload.id = user_count;
    Todo.push(request.payload);
      reply(Todo);
  },
  config: {
    validate: {
      payload: {
        data:Joi.array()
        // consumerNumber: Joi.string().min(12).max(12),
        // billingUnit: Joi.string().min(3).max(4),
        // connectionType: Joi.string().min(2).max(2)
      }
    }
  }
});

server.register([{
    register: Good,
    options: {
        reporters: {
            console: [{
                module: 'good-squeeze',
                name: 'Squeeze',
                args: [{
                    response: '*',
                    log: '*'
                }]
            }, {
                module: 'good-console'
            }, 'stdout']
        }
    }
},
  {
    register: Blipp
    }
  ], (err) => {

    if (err) {
        throw err; // something bad happened loading the plugin
    }
});

server.start((err) => {

    if (err) {
        throw err;
    }
    console.log(`Server running at: ${server.info.uri}`);
});
