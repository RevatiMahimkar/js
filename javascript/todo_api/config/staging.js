'use strict';

module.exports = {
  env: 'staging',

  product: {
    name: '3'
  },

  server: {
    3: {
      host: '0.0.0.0',
      port: process.env.PORT || 3000,
      tls: false
    }
  },

  chairo: {
    options: {
      // prevent seneca timeout error
      // https://github.com/senecajs/seneca-transport/issues/23
      timeout: 999999999
    }
  },

  cache: {
    mainCache: {
      engine: 'catbox-memory',
      name: 'mainCache',
      partition: '3'
    }
  },

  dogwater: {
    connections: {
      myApi: {
        adapter: 'sails-mongo',
        host: 'localhost',
        port: 27017,
        database: 'my-api'
      }
    },
    adapters: {
      'sails-mongo': require('sails-mongo')
    },
    models: require('path').resolve(__dirname, '..', 'models')
  },

  jobs: {
    address: 'mongodb://localhost:27017/my-api',
    collection: 'jobs',
    frequency: '5 minutes',
    concurrency: 20
  },

  apiPrefix: '/api/v1',

  deploy: {
    branch: 'develop',
    servers: 'saurabh.ghewari@vm-address-here.com',
    deployTo: '/home/saurabh.ghewari/3'
  }
};
