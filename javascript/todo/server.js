'use strict';

var Hapi = require('hapi');
var Good = require('good');
var Todo= [];
var task=[];
var id = [];
var user_count= 0;
var task_count= 0;
var Joi = require('joi');
var Blipp= require('blipp');

// Create a new hapi server
var server = new Hapi.Server();
server.connection({
    host: 'localhost',
    port: 3000
});

// Get User by id
server.route({
  method: 'GET',
  path: '/api/v1/users/{id}',
  handler: function(request, reply) {
    var id = Number(request.params.id);
    var user=Todo.filter(function(num){
      return (num.id === id)
    })
    reply(user[0])
  },
});

// Get User List
server.route({
  method: 'GET',
  path: '/api/v1/users',
  handler: function(request, reply) {
    return reply(Todo)
  }
});

// Create Users
server.route({
  method: 'POST',
  path: '/api/v1/users',
  handler: function(request, reply) {
    user_count++;
    request.payload.id = user_count;
    Todo.push(request.payload);
      reply(Todo);
  },
  config: {
    validate: {
      payload: {
        name: Joi.string().min(3).max(20),
        email: Joi.string().min(3).max(20),
        mobile: Joi.string().min(10).max(10),
        pincode: Joi.string().min(6).max(6)
      }
    }
  }
});

// Edit User
// server.route({
//   method: 'PUT',
//   path: '/api/v1/users/{id}',
//   handler: function(request, reply) {
//     console.log("called");
//      var mem_id = Number(request.params.id);
//      console.log(mem_id);
//      var editUser =  Todo.filter(function (todo) {
//        return todo.id === mem_id;
//      })
//         console.log(editUser);
//          return reply(editUser[0]);
//   },
//   config: {
//     validate: {
//       payload: {
//         name: Joi.string().min(3).max(20),
//         email: Joi.string().min(3).max(20),
//         mobile: Joi.string().min(10).max(10),
//         pincode: Joi.string().min(6).max(6)
//       }
//     }
//   }
// });

// Delete User
server.route({
  method: 'DELETE',
  path: '/api/v1/users/{id}',
  handler: function(request, reply) {
    var user_id = Number(request.params.id) - 1;
    var delUser =  Todo.splice(user_id,1)
        return reply(Todo);
  },
});

// Create Tasks
server.route({
  method: 'POST',
  path: '/api/v1/tasks',
  handler: function(request, reply) {
    task_count++;
    request.payload.id = task_count;
    task.push(request.payload);
    reply(task);
  },
  config: {
    validate: {
      payload: {
        userid: Joi.number().integer().positive().min(1).required(),
        title: Joi.string().min(3).max(20),
        description: Joi.string().min(5).max(100),
        date: Joi.string().min(10).max(10)
      }
    }
  }
});

//Get Task by id
server.route({
  method: 'GET',
  path: '/api/v1/tasks/{id}',
  handler: function(request, reply) {
    var task_id = Number(request.params.id);
    var tasksarr=task.filter(function(name){
      return (name.id === task_id)
    })
    reply(tasksarr[0])
  },
});

// Get Tasks List
server.route({
  method: 'GET',
  path: '/api/v1/tasks',
  handler: function(request, reply) {
    return reply(task)
  }
});

// //Edit Task
// server.route({
//   method: 'PUT',
//   path: '/api/v1/tasks',
//   handler: function(request, reply) {
//     task_count++;
//     request.payload.id = task_count;
//     task.push(request.payload);
//     reply(task);
//   },
//   config: {
//     validate: {
//       payload: {
//         userid: Joi.number().integer().positive().min(1).required(),
//         title: Joi.string().min(3).max(20),
//         description: Joi.string().min(5).max(100),
//         date: Joi.string().min(10).max(10)
//       }
//     }
//   }
// });

//Filter Tasks by Userid
server.route({
  path:'/users/{id}/tasks',
  method: 'GET',
  handler: (request, reply) => {
    var userid = Number(request.params.id);
    var userTasks = task.filter(function (val) {
      return (val.userid === userid);
    })
    reply(userTasks);
  }
});

// Delete Task
server.route({
  method: 'DELETE',
  path: '/api/v1/tasks/{id}',
  handler: function(request, reply) {
    var taskid = Number(request.params.id) - 1;
    var delTask =  task.splice(taskid,1)
        return reply(task);
  },
});

    // -----------------------------------------------------
server.register([{
    register: Good,
    options: {
        reporters: {
            console: [{
                module: 'good-squeeze',
                name: 'Squeeze',
                args: [{
                    response: '*',
                    log: '*'
                }]
            }, {
                module: 'good-console'
            }, 'stdout']
        }
    }
},
  {
    register: Blipp
    }
  ], (err) => {

    if (err) {
        throw err; // something bad happened loading the plugin
    }
});

    server.start(function() {
      console.log('info', 'Server running at: ' + server.info.uri);
    });
