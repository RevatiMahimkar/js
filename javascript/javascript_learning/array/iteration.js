
// forloop

var arr3, i, num, total;
arr3 = [4, 8, 15, 16, 23, 42];
total = 0;
for (i = 0; i < arr3.length; i++) {
	num = arr3[i];
	total = total + num;
}
console.log(total);

// forEach

var arr4,total;
arr4 = [4, 8, 15, 16, 23, 42];
total = 0;
arr4.forEach(function (num) {
	total = total + num
});
console.log(total);

// Map

var arr5;
arr5 = [4, 8, 15, 16, 23, 42];
var arr6 = arr5.map(function (num) {
	  return (num * num);
});
console.log(arr6);

// Every

var arr0, isValid;
arr0 = [1, 2, 3, 4, 5, 6];
arr0.every(function(num){
	console.log (num < 10);
})

// Some

var arr1, isValid;
arr1 = [1, 2, 3, 4, 5];
arr1.some(function(num){
	console.log (num > 3);
})

// Filter

var status = [{name: "Revati", active: true},
			  {name: "Puja", active: false},
			  {name: "Pallavi", active: true},
			  {name: "Pratibha", active: false},
			  {name: "Rutuja", active: true}];
Filter=status.filter(function(val){
	return val.active === false;
});
console.log(Filter);

// Filter User

var status = [{name: "Revati", active: true},
			  {name: "Puja", active: false},
			  {name: "Pallavi", active: true},
			  {name: "Pratibha", active: false},
			  {name: "Rutuja", active: true}];
var Filter=	[];
status.filter(function(val){
	if(val.active) Filter.push(val.name);
});
console.log(Filter);


// Reduce

var arr7, total;
arr7 = [1, 2, 3, 4, 5];
total = arr7.reduce(function(previous, current) {
return previous + current;
});
console.log(total);

// Reduce Right

var arr8, total;
arr8 = [1, 2, 3, 4, 5];
total = arr8.reduceRight(function(previous, current) {
return previous + current;
});
console.log(total);