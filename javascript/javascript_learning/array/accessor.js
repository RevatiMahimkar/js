// Concat

var arr1, arr2, arr3, array 
arr1 = [1, 2, 3];
arr2 = [4, 5, 6];
arr3 = [7, 8, 9];
array = arr1.concat(arr2, arr3);
console.log(array);

// Join

var arr1, arr2, arr3, array 
arr1 = [1, 2, 3];
arr2 = [4, 5, 6];
arr3 = [7, 8, 9];
array = arr1.concat(arr2, arr3);
console.log(array.join(" + ") + " = " + "45 ");

// toSting

var myName = ["Hello", "!", "This", "is", "Revati"];
console.log(myName.toString());

// Slice

var arr = [1, 2, 3];
arr1 = arr;
arr1[1] = "+";
console.log(arr.slice(1, 2))

// indexOf

var arr1, arr2, arr3, array 
arr1 = [1, 2, 3];
arr2 = [4, 5, 6];
arr3 = [7, 8, 9];
array = arr1.concat(arr2, arr3);
console.log(array.indexOf(5));

// lastIndexOfs

var arr1, arr2, arr3, array 
arr1 = [1, 2, 3];
arr2 = [4, 5, 6];
arr3 = [7, 8, 9];
array = arr1.concat(arr2, arr3);
console.log(array.lastIndexOf(5));