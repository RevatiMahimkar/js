// Push

var num = [1, 2, 3, 4];
num.push(5);
console.log(num);

// Pop

var num = [1, 2, 3, 4, 5];
num.pop();
console.log(num);

// Reverese

var num = [1, 2, 3, 4, 5];
num.reverse();
console.log(num);

// Sort

var num = [5, 4, 3, 2, 1];
num.sort(num);
console.log(num);

// Shift

var num = [1, 2, 3, 4, 5];
num.shift();
console.log(num);

// Unshift

var num = [1, 2, 3, 4, 5];
num.unshift(0);
console.log(num);

// Splice

var num = [1, 2, 3, 4, 5];
num.splice(0, 1, 0, 1);
console.log(num);