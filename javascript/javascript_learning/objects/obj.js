
// Creating an Object

var  profile;
profile = {
firstName: "Hugo",
lastName: "Reyes",
flight: "Oceanic 815",
car: "Camaro"
};

// Reading from an Object

var  profile;
profile = {
firstName: "Hugo",
lastName: "Reyes",
flight: "Oceanic 815",
car: "Camaro"
};
console.log(profile.firstName);    //dot notation
console.log(profile["lastName"]);  //bracket notation

// Adding to an Object

var fruitQuantity = {};
fruitQuantity.Apple = 15;
fruitQuantity["Mango"] = "1 dozens";
console.log(fruitQuantity);

// ------Nested Object ------ //

var person;
person = {
name: {
first: "Hugo",
last: "Reyes"
}
};

// Reading from an Object

var person;
person = {
name: {
first: "Hugo",
last: "Reyes"
}
};
console.log(person.name.first);

// Adding to an Object

var fruitQuantity = {};
fruitQuantity.apple = {};
fruitQuantity.apple.red = 15;
fruitQuantity.apple.green = 25;
fruitQuantity.mango = {};
fruitQuantity.mango.hapus = 35;
fruitQuantity.mango.payari = 45;
console.log(fruitQuantity);

// Nemespacing

var Projects = {firstName: "Revati"};
Projects.Strings = {};
Projects.Strings.Warning = {};
Projects.Strings.Warning.sessionExpired = "Your session has expired.";
Projects.Strings.Warning.overQuota = "You have exceeded your quota!"
Projects.Strings.Warning.outOfStock = "We are out of stock!"
console.log(Projects);

var Project = Project || {};
console.log(Project);

// Looping over an object

var data, key;
data = {
firstName: "Revati",
lastName: "Mahimkar",
occupation: "QA"
};
for (key in data) {
console.log(key + " is " + data[key]);
}