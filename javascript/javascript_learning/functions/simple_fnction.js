
// Simple Fuction Calling

function welcome(){
	console.log("Welcome");
}
welcome();

// Function with Arguments

function sayHello(msg){
	console.log(msg);
}
sayHello("Welcome");

// Scope

function hi() {
var hello = "hello"
console.log(hello);
};
hi();
// console.log(hello);

// nested functions

function fullName() {
var firstName = "Revati";
function middleName(){
var middleName= "Ashok";
function consoleFullName() {
var lastName = "Mahimkar";
console.log("Full name: " + firstName + " " + middleName + " " + lastName);
}
consoleFullName();
}
middleName();
}
fullName();

// Nested Functon eg
function levela() {
var a = "a";
function levelb() {
var b = "b";
function levelc(){
var c= "c";
function leveld() {
var d = "d";
console.log("leveld", a, b, c, d);
}
leveld();
console.log("levelc", a, b, c);
}
levelc();
console.log("levelb", a, b);
}
levelb();
console.log("levela", a);
}
levela();

// // Hoisting 

// var name = "Emma";
// function nameHer() {
// console.log(name); // outputs undefined
// var name;
// name = "Audrey";
// }
// nameHer();

/////// Function declarations ////////
// Function Declaration

console.log(hi());
function hi() {
return "Hi!";
}

// // Function Expression

// console.log(hey());
// var hey = function () {
// return "Hey.....!!!!";
// }

// Arguments - 1

function person(firstName, lastName, age) {
console.log(firstName);
console.log(lastName);
console.log(age);
}
person("Revati", "Mahimkar", 23);

// Arguments - 2

function concatenate() {
var i, str;
str = "";
for (i = 0; i < arguments.length; i += 1) {
str += arguments[i];s
}
return str;
}concatenate("Super", "cali", "fragilistic", "expiali", "docious");