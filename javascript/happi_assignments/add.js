exports.register = function (server, options, next) {
  server.route({
      method: 'GET',
      path: '/add/{user*2}',
      handler: function (request, reply) {
          const userParts = request.params.user.split('/');
          var a  = Number(encodeURIComponent(userParts[0]));
          var b = Number(encodeURIComponent(userParts[1]));
          reply(a + b);
        }
      });
      next();
};
exports.register.attributes = {
    pkg: require('./package.json')
};
