'use strict';

const Hapi = require('hapi');
const Good = require('good');
const Blipp = require('blipp');
// Create a server with a host and port
const server = new Hapi.Server();
server.connection({
    host: 'localhost',
    port:8000
});

// Add the route
// server.route({
//     method: 'GET',
//     path: '/add/{user*2}',
//     handler: function (request, reply) {
//         const userParts = request.params.user.split('/');
//         reply('The '+ 'addition ' + 'of ' + 'number ' + encodeURIComponent(userParts[0]) + '& ' +' ' + encodeURIComponent(userParts[1]) + ' is ' +  ' 3' + '.');
//     }
// });

server.register([{
    register: Good,
    options: {
        reporters: {
            console: [{
                module: 'good-squeeze',
                name: 'Squeeze',
                args: [{
                    response: '*',
                    log: '*'
                }]
            }, {
                module: 'good-console'
            }, 'stdout']
        }
    }
},
// {
//  register: require('./add.js')
// },
    {
  register: require('./user_response_arr.js')
    },
  {
    register: Blipp
    }], (err) => {

    if (err) {
        throw err; // something bad happened loading the plugin
    }

// Start the server
server.start((err) => {

    if (err) {
        throw err;
    }
    console.log('Server running at:', server.info.uri);
  });
});
