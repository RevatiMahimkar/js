exports.register = function (server, options, next) {
// console.log("declared an arr");
var arr = [];
server.route({
    method: 'GET',
    path: '/users',
    handler: function (request, reply) {
        // console.log("pushed values in arr");
        arr(request.params.users);
        // console.log(arr);
         return reply(arr);
        }
      });
server.route({
    method: 'POST',
    path: '/users',
    handler: function (request, reply) {
        // console.log("pushed values in arr");
        arr.push(request.payload);
        // console.log(arr);
         return reply(arr);
        }
      });
server.route({
    method: 'DELETE',
    path: '/{users?}',
    handler: function (request, reply) {
              // console.log(typeof request.params.users);
              arr.splice(request.params.users, 1);
              // console.log(arr);
              return reply(arr);
              }
            });
            next();
};
exports.register.attributes = {
    pkg: require('./package.json')
};
