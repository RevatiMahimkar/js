var webdriver = require('selenium-webdriver'),
    By = require('selenium-webdriver').By,
    until = require('selenium-webdriver').until;

var driver = new webdriver.Builder()
    .forBrowser('firefox')
    .build();

driver.get('http://demo.mahara.org');
driver.findElement(By.id('login_login_username')).sendKeys('student1');
driver.findElement(By.id('login_login_password')).sendKeys('Testing1');
driver.findElement(By.id('login_submit')).click();

driver.findElement(By.linkText('Settings')).then(function(element) {
  console.log('Yes, found the element');
}, function(error) {
  console.log('The element was not found, as expected');
});
driver.quit();
