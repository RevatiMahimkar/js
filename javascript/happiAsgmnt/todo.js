'use strict';

export.register = function getFormData() {
    var task = document.getElementById("task").value;
    if (checkInputText(task, "Please enter a task")) return;

    var who = document.getElementById("who").value;
    if (checkInputText(who, "Please enter a person to do the task")) return;

    var date = document.getElementById("dueDate").value;
    if (checkInputText(date, "Please enter a due date")) return;
    // later, process date here

    var latitude=null;
    var longitude=null;

    var id = (new Date()).getTime();
    var todoItem = new Todo(id, task, who, date, latitude, longitude);
    todos.push(todoItem);
    addTodoToPage(todoItem);
    saveTodoItem(todoItem);
    findLocation(todoItem);
    sendLocation(todoItem);
    // hide search results
    hideSearchResults();
}
