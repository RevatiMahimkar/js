'use strict';

const Hapi = require('hapi');
const Good = require('good');
const Blipp = require('blipp');
const server = new Hapi.Server();
server.connection({ port: 8000, host: 'localhost' });

server.route({
    method: 'GET',
    path: '/',
    handler: function (request, reply) {
        reply('Hello World..!!');
    }
});

server.route({
    method: ['PUT','POST'],
    path: '/hello',
    handler: function (request, reply) {
        reply('I did something');
    }
});

server.route({
    method: 'GET',
    path: '/{name}',
    handler: function (request, reply) {
        reply('Hello, ' + encodeURIComponent(request.params.name) + '!');
    }
});

server.route({
    method: 'GET',
    path: '/hello/{user?}',
    handler: function (request, reply) {
        const user = request.params.user ? encodeURIComponent(request.params.user) : 'stranger';
        reply('This ' + 'is '+ user + ' .' + ' !');
    },
});

// config: {
//         description: 'Say hello!',
//         notes: 'The user parameter defaults to \'stranger\' if unspecified',
//         tags: ['api', 'greeting']
//     }
// });
server.route({
    method: 'GET',
    path: '/hello/{user*}',
    handler: function (request, reply) {
        const userParts = request.params.user.split('/');
        reply('Hello ' + encodeURIComponent(userParts[0]) + ' ' + encodeURIComponent(userParts[1]) + ' ' + encodeURIComponent(userParts[2]) + '!');
    }
});

server.register([{
    register: Good,
    options: {
        reporters: {
            console: [{
                module: 'good-squeeze',
                name: 'Squeeze',
                args: [{
                    response: '*',
                    log: '*'
                }]
            }, {
                module: 'good-console'
            }, 'stdout']
        }
    }
},
// { register: require('./myplugin'),
//       options: {
//         message: 'hello'
//       }
//   },{
//       register: require('./route')
//     },{
//         register: Blipp,
//       }

], (err) => {

      if (err) {
          throw err;
      }
    // if (err) {
    //     throw err; // something bad happened loading the plugin
    // }


    server.start((err) => {

        if (err) {
            throw err;
        }
        server.log('info', 'Server running at: ' + server.info.uri);
    });
});
