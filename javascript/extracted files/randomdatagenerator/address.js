var users = [];
var Things = 200;
var moment = require("moment");
var faker = require("faker");
var _ = require("lodash");

for (var i = 0; i < Things; i++) {
    users.push({

        "Email": faker.internet.email(),
        "username": faker.internet.userName(),
        "AltPh": faker.phone.phoneNumberFormat(),
        "password": faker.internet.password(),
        "Zip": faker.address.zipCode(),
        "State": faker.address.state(),
        "Country": faker.address.country(),
        // "TimeZone": faker.address.timeZone(),
        "latitude": faker.address.latitude(),
        "longitude": faker.address.longitude(),
        "City": faker.address.city(),
        "StreetName": faker.address.streetName(),
        "StreetAddress": faker.address.streetAddress(),
        "SecondaryAddress": faker.address.secondaryAddress(),
        // "BuildingNumber": faker.address.buildingNumber()
    });
}

var userkeys = ["Email", "username", "password", "AltPh", "Zip", "State", "Country", "TimeZone", "latitude", "longitude", "City", "StreetName", "Streetaddress", "SecondaryAddress", "BuildingNumber"];
var uservalues = users.map(function(user) {
    return _.values(user);
});

var lineArray = [];
// lineArray.push(userkeys.join(","));

uservalues.forEach(function(infoArray, index) {
    var line = infoArray.join(", ");
    lineArray.push(line);
});

var csvContent = lineArray.join("\n");


require("fs").writeFile("address.csv", csvContent, function(err) {});

// var arr = uservalues.map(function(v){ return v.join(',')}).join(',\n');

// // console.log(arr);
