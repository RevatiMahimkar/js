var users = [];
var Things = 200;
var moment = require("moment");
var faker = require("faker");
var _ = require("lodash");

for (var i = 0; i < Things; i++) {
    users.push({

        // "name": faker.Name(),
        // "name_with_middle": faker.name.middleName(),
        "first_name": faker.name.firstName(),
        "last_name": faker.name.lastName(),
        "prefix": faker.name.prefix(),
        "suffix": faker.name.suffix(),
        // "title": faker.name.title(),
        "title": _.sample(["Manager", "Employee", "Driver"]),
        "date": faker.date.recent(),
        "company": faker.company.companyName(),
        // "suffixes": faker.company.suffixes(),
        "avatar": faker.image.avatar()
    });
}

var userkeys = ["first_name", "last_name", "prefix", "suffix", "title", "date", "company", "avatar"];
var uservalues = users.map(function(user) {
    return _.values(user);
});

var lineArray = [];
// lineArray.push(userkeys.join(","));

uservalues.forEach(function(infoArray, index) {
    var line = infoArray.join(", ");
    lineArray.push(line);
});

var csvContent = lineArray.join("\n");


require("fs").writeFile("name.csv", csvContent, function(err) {});

// var arr = uservalues.map(function(v){ return v.join(',')}).join(',\n');

// // console.log(arr);
