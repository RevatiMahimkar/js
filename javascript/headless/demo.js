// Verified Consumers List
// await page.goto('http://192.168.1.237:6008/consumers');

// Select a Head
// await page.click('#wrap > div.container-fluid.b-b-none.parent-tab-content > div.tab-container.tab-midnightblue.tab-normal > div > div.tab-pane.active > div.row.m-t-15 > div > form:nth-child(1) > div:nth-child(1) > select');

// Go to Consumer Detailed View
// await page.click('#wrap > div.container-fluid.b-b-none.parent-tab-content > div.tab-container.tab-midnightblue.tab-normal > div > div.tab-pane.active > div.container-fluid > div > div > panel > div > div.panel-body > div > table > tbody > tr:nth-child(1) > td:nth-child(1)');

// Wait for delay on Detail View
// await page.waitFor(1000);

// Go back to Verified Consumers List
// await page.goto('http://192.168.1.237:6008/consumers');

// Select Next Page
await page.click('#wrap > div.container-fluid.b-b-none.parent-tab-content > div.container-fluid.row.m-b-2em > div > div > form > div.col-sm-4.m-t-15.m-l-15.p-t.text-right.dataTables_paginate.paging_bootstrap_extended.p-t.text-right.custom-pagination.pull-right > div > a.btn.btn-sm.default.next > em');

// Focus on Search Box
await page.focus('#wrap > div.container-fluid.b-b-none.parent-tab-content > div.tab-container.tab-midnightblue.tab-normal > div > div.tab-pane.active > div.row.m-t-15 > div > form.ng-pristine.ng-valid.ng-valid-minlength.ng-valid-maxlength > div > div > input');

// Type a Search Query
await page.type('123456789012');

// Click of Search Button
await page.click('#wrap > div.container-fluid.b-b-none.parent-tab-content > div.tab-container.tab-midnightblue.tab-normal > div > div.tab-pane.active > div.row.m-t-15 > div > form.ng-dirty.ng-valid.ng-valid-parse.ng-valid-minlength.ng-valid-maxlength > div > button.btn.btn-primary.theme-primary-blue-color-bg');

// Click of Clear Button
await page.click('#wrap > div.container-fluid.b-b-none.parent-tab-content > div.tab-container.tab-midnightblue.tab-normal > div > div.tab-pane.active > div.row.m-t-15 > div > form.ng-dirty.ng-valid.ng-valid-parse.ng-valid-minlength.ng-valid-maxlength > div > button:nth-child(3)');

// Consumer Summary List
await page.goto('http://192.168.1.237:6008/consumers_summary');

// Tile Total Consumer Selection
await page.click('#wrap > div.container-fluid.b-b-none.parent-tab-content > div:nth-child(15) > div.row.m-t-20 > div:nth-child(1) > div > div:nth-child(1) > a > div > div.col-sm-4.col-md-5.col-lg-6.col-xl-4.p-a-0.f-34.l-h-40.text-right');

// Focus on Search Box
await page.focus('#wrap > div.container-fluid.b-b-none.parent-tab-content > div:nth-child(15) > div.row.m-t-20 > div.col-xl-12.col-lg-12.col-md-12.col-sm-12.col-xs-12.p-l-0.m-t-10.p-r-0 > div > form > div > input');

// Type a Search Query
await page.type('123456789012');

// Click of Search Button
await page.click('#wrap > div.container-fluid.b-b-none.parent-tab-content > div:nth-child(15) > div.row.m-t-20 > div.col-xl-12.col-lg-12.col-md-12.col-sm-12.col-xs-12.p-l-0.m-t-10.p-r-0 > div > form > button.btn.btn-primary.theme-primary-blue-color-bg');

// Click of Clear Button
await page.click('#wrap > div.container-fluid.b-b-none.parent-tab-content > div:nth-child(15) > div.row.m-t-20 > div.col-xl-12.col-lg-12.col-md-12.col-sm-12.col-xs-12.p-l-0.m-t-10.p-r-0 > div > form > button:nth-child(3)');

// Select a Ward
await page.click('#wrap > div.container-fluid.b-b-none.parent-tab-content > div:nth-child(11)');
