const puppeteer = require('puppeteer');

(async() => {
  const browser = await puppeteer.launch({
    headless: false,
    slowMo: 200
  });
  const page = await browser.newPage();

  // Open page.
  await page.goto('https://wss.mahadiscom.in/wss/wss?uiActionName=getCustAccountLogin');

  await page.setViewport({
    width: 1920,
    height: 1080
  });
  // Focus input field & Type in query.
  await page.type('#loginId','tmc.billwise');

  // Focus input field & Type in query.
  await page.type('#password','Selenite#1');

  // Submit the form.
  await page.click('#lblLoginButton');

  // await page.goto('https://wss.mahadiscom.in/wss/wss');
  // // Select Connection Type
  await page.type('#consumerType', 'LT Consumer');

  // // Enter a Consumer Number
  await page.type('#consumerNumber', '019838383812');
  //
  // // Select a BU
  await page.type('#BU', '0671');
  //
  // // Click on Add Button
  await page.click('#lblAdd1');

  // Select a Consumer
  await page.click('#radCustList','000010210305_4542');

  await page.click('#grdCustList_ctl02_viewHTMLBill');

  // await page.click('#Label4');
  // await page.click('#lblServiceRequestStatus');
  // await page.click('#lblDisconnectionRequest');
  // await page.click('#lblServiceRequest');
  // await page.click('#lblBtn1');
  // await page.click('#Label1');
  // // check History
  // await page.click('#Label2');
  // await page.click('#Label3');
  // Keep the browser open.
  // await browser.close();
  await page.click('#topnav_hreflogout');

})();
