module.exports = function() {

  addConsumer();
  function addConsumer(){

  	var consumerNumber = document.getElementById("consumerNumber").value;
  	var BU = document.getElementById("BU").value;
  	var circleCode=document.getElementById("ddlCircleCode").value;
  	var ConsumerType=document.getElementById("consumerType").value;
  	var AccountId=document.getElementById("hdnAccountId").value;
  	var AccountType=document.getElementById("isLT").value;
  	var sessionTimeout=false;
      var params;
      var out =
      {
          resp: function validateSession(res)
          {

              var JsonObj = eval("(" + res + ")");
              if(JsonObj.toString() == "false")
              {
                 sessionTimeout=true;
                 // addToErrorList(sessionValidationMessage);
              }
              else if(JsonObj.toString().substring(0,5) == "Error")
              {
               sessionTimeout=false;
               // addToErrorList(JsonObj.toString());
              }
              else
              {
                  sessionTimeout=false;
                  if(ConsumerType == -1)
                  {
                    // addToErrorList(eConsumerType);
                  }
                  else
                  {consumerType
                      if(ConsumerType == 1)
                      {
                              if(isEmpty(consumerNumber))
  	                        {
  		                        // addToErrorList(eConsumerNumber);
  	                        }
  	                        if(BU == -1)
  	                        {
  		                        // addToErrorList(eBU);
  	                        }
  	                        if(consumerNumber != "" && BU != -1)
                              {
                                  var params = "ConsumerNo=" + consumerNumber + "&BuNumber=" +BU + "&consumerType=" +ConsumerType;
                                  var out =
                                  {
                                      resp: function consumerInfo(res)
                                      {

                                          var JsonObj = eval("(" + res + ")");

                                          if(JsonObj.toString().substring(0,5) =="Error")
                                          {
                                              // addToErrorList(JsonObj.toString());
                                          }
                                          else if(JsonObj.toString() == "false")
                                          {
                                               // addToErrorList("Given combination of Consumer Number ,Consumer Type and BU does not match");
                                          }
                                          else
                                          {
                                              var params = "ConsumerNo=" + consumerNumber  +"&accountId="+AccountId;
                                              var out =
                                              {
                                                  resp: function consumerInfo(res)
                                                  {

                                                      var JsonObj = eval("(" + res + ")");
                                                      if(JsonObj !="")
                                                      {
                                                          // addToErrorList(JsonObj.toString());
                                                      }

                                                  }
                                              }
                                              callAjax("POST", "wss?uiActionName=postValidateConsumerNo&IsAjax=true", params, false, out.resp);
                                          }

                                      }
                                  }
                                  callAjax("POST", "wss?uiActionName=validateConsumerNumberHTLT&IsAjax=true", params, false, out.resp);
                              }//end of if consumer an bu condition check
                       }
                       else
                       {
                          if(isEmpty(consumerNumber))
                          {
  	                        // addToErrorList(eConsumerNumber);
                          }
                          if(circleCode == -1)
                          {
                            // addToErrorList("Please select circle Code");
                          }
                          if(consumerNumber != "" && circleCode != -1)
                          {
                              var params = "ConsumerNo=" + consumerNumber  + "&consumerType=" + ConsumerType+"&CircleCode="+circleCode;
                              var out =
                              {
                                  resp: function consumerInfo(res)
                                  {

                                      var JsonObj = eval("(" + res + ")");
                                      if(JsonObj.toString().substring(0,5) =="Error")
                                      {
                                          // addToErrorList(JsonObj.toString());
                                      }
                                      else if(JsonObj.toString() == "false")
                                      {
                                           // addToErrorList("Given combination of Consumer Number ,Consumer Type and Circle Code does not match");
                                      }
                                      else
                                      {
                                              var params = "ConsumerNo=" + consumerNumber  +"&accountId="+AccountId;
                                              var out =
                                              {
                                                  resp: function consumerInfo(res)
                                                  {

                                                      var JsonObj = eval("(" + res + ")");
                                                      if(JsonObj !="")
                                                      {
                                                          // addToErrorList(JsonObj.toString());
                                                      }

                                                  }
                                              }
                                              callAjax("POST", "wss?uiActionName=postValidateConsumerNo&IsAjax=true", params, false, out.resp);

                                      }
                                  }
                              }
                              callAjax("POST", "wss?uiActionName=validateConsumerNumberHTLT&IsAjax=true", params, false, out.resp);
                          }
                       }
                  }//end of else part of conumertype
              }//end of else part of validate session
          }//end of resp
      }//end of out
      callAjax("POST", "wss?uiActionName=validateSession&IsAjax=true", params, false, out.resp);


  	if(isErrorListEmpty())
  	{

  		var params = "ConsumerNo=" + consumerNumber + "&BuNumber=" +BU +"&AccountID="+ AccountId+"&CircleCode="+circleCode;
  		var out =
          {

              resp: function consumerInfo(res) {

                  var JsonObj = eval("(" + res + ")");
                  if (JsonObj.toString().substring(0, 5) != "error") {
                      document.getElementById("consumerNumber").value = "";
                      document.getElementById("BU").value = "-1";
                      var table = document.getElementById("grdCustList");
                      document.getElementById("noRecordsTable").style.display = 'none';
                      if (table == null) {
                          // alert(eAddConsumerSuccesfully);
                          var custTable = document.createElement("Table");
                          custTable.setAttribute("id", "grdCustList");
                          custTable.setAttribute("cellpadding", "0");
                          custTable.setAttribute("cellspacing", "0");
                          custTable.setAttribute("width", "100%");
                          custTable.setAttribute("runat", "server");
                          custTable.setAttribute("class", "list_table");
                          custTable.setAttribute("style", "width: 100%; border-collapse: collapse;");
                          var newRow = custTable.insertRow(0);
                          var tbody = [];

                          tbody[0] = document.createElement('th');
                          tbody[0].scope = "col";

                          tbody[1] = document.createElement('th');
                          tbody[1].scope = "col";
                          tbody[1].innerHTML = "Consumer No.";

                          tbody[2] = document.createElement('th');
                          tbody[2].className = "right";
                          tbody[2].scope = "col";
                          tbody[2].innerHTML = "BU";

                          tbody[3] = document.createElement('th');
                          tbody[3].className = "left";
                          tbody[3].scope = "col";

                          tbody[4] = document.createElement('th');
                          tbody[4].className = "right";
                          tbody[4].scope = "col";
                          tbody[4].innerHTML = "Bill Month";

                          tbody[5] = document.createElement('th');
                          tbody[5].className = "right";
                          tbody[5].scope = "col";
                          tbody[5].innerHTML = "Consumption";

                          tbody[6] = document.createElement('th');
                          tbody[6].className = "right";
                          tbody[6].scope = "col";
                          tbody[6].innerHTML = "Bill Amount";

                          tbody[7] = document.createElement('th');
                          tbody[7].className = "right";
                          tbody[7].scope = "col";
                          tbody[7].innerHTML = "Bill Due Date";

                          tbody[8] = document.createElement('th');
                          tbody[9] = document.createElement('th');
                          tbody[9].className = "center";
                          tbody[9].scope = "col";
                          tbody[9].innerHTML = "View Bill";

                          for (var i = 0; i < tbody.length; i++) {
                              newRow.appendChild(tbody[i]);
                          }
                          //debugger  ;

                          var newRow1 = custTable.insertRow(-1);
                          var firstCell1 = newRow1.insertCell(-1);
                          var secondCell1 = newRow1.insertCell(-1);
                          var thirdCell1 = newRow1.insertCell(-1);
                          var forthCell1 = newRow1.insertCell(-1);
                          var fifthCell1 = newRow1.insertCell(-1);
                          var sixthCell1 = newRow1.insertCell(-1);
                          var seventhCell1 = newRow1.insertCell(-1);
                          var eighthCell1 = newRow1.insertCell(-1);
                          var ninthCell1 = newRow1.insertCell(-1);
                          var tenthCell1 = newRow1.insertCell(-1);
                          var eleventhCell1 = newRow1.insertCell(-1);
                          var tewelvthCell1 = newRow1.insertCell(-1);
                          firstCell1.innerHTML = "<input  name='radCustList' id='radCustList' type='radio'  onclick='radioCheck(this);'/>";
                          secondCell1.innerHTML = JsonObj.billingConsumptionDTO[0].consumerNo;

                          thirdCell1.innerHTML = JsonObj.billingConsumptionDTO[0].billingUnit;
                          thirdCell1.className = "right";

                          forthCell1.innerHTML = JsonObj.billingConsumptionDTO[0].billingUnitDesc
                          forthCell1.className = "left";

                          fifthCell1.innerHTML = JsonObj.billingConsumptionDTO[0].billMonth;
                          fifthCell1.className = "right";

                          sixthCell1.innerHTML = JsonObj.billingConsumptionDTO[0].consumptionUnits;
                          sixthCell1.className = "right";

                          seventhCell1.innerHTML = JsonObj.billingConsumptionDTO[0].billAmount.toFixed(2);
                          seventhCell1.className = "right";

                          eighthCell1.innerHTML = JsonObj.billingConsumptionDTO[0].dueDate;
                          eighthCell1.className = "right";

                          ninthCell1.innerHTML = "<input   type='hidden'" + " value='" + JsonObj.billingConsumptionDTO[0].billToBePaid + "' />";
                          //Modified By:Rajashree
                          //Date: 19 OCT 16
                          // Reason:  To enable payment button for PD consumers and display HTML & PDF bill for PD consumers (Consumer Status = 9).

                          //Modified By:Rajashree
                          //Date: 19 APR 17
                          // Reason:  To disable payment button and To display HTML & PDF bill for Paid Pending consumers (Consumer Status = 8).
                          if (JsonObj.billingConsumptionDTO[0].billMonth != null) {

                              // if(JsonObj.billingConsumptionDTO[0].consumerStatus == "9")
                              if (JsonObj.billingConsumptionDTO[0].consumerStatus == "8") {
                                  tenthCell1.innerHTML = "<img style='cursor: pointer; cursor: hand;display:none;' title='View HTML' src='images/ieicon.png'  id='Img1'   width='16' height='16'  onclick='popUpPrintBillHTML(this);'/>"
                              }
                              else {
                                  tenthCell1.innerHTML = "<img style='cursor: pointer; cursor: hand;' title='View HTML' src='images/ieicon.png'  id='Img1'   width='16' height='16'  onclick='popUpPrintBillHTML(this);'/>"
                              }
                          }

                          eleventhCell1.innerHTML = "<input   type='hidden'" + " value='" + JsonObj.billingConsumptionDTO[0].SDToBePaid + "' />";

                          //added for Checking PD consumers If Consume status= P.D then Y else N
                          //Modified By:Rajashree
                          //Date: 19 OCT 16
                          // Reason:  To enable payment button for PD consumers and display HTML & PDF bill for PD consumers (Consumer Status = 9).

                          //Modified By:Rajashree
                          //Date: 19 APR 17
                          // Reason:  To disable payment button and To display HTML & PDF bill for Paid Pending consumers (Consumer Status = 8).
                          if (JsonObj.billingConsumptionDTO[0].consumerStatus == "9") {
                              tewelvthCell1.innerHTML = "<input   type='hidden'  value='9' />";
                          }
                          else if (JsonObj.billingConsumptionDTO[0].consumerStatus == "8") {
                              tewelvthCell1.innerHTML = "<input   type='hidden'  value='8' />";
                          }
                          else {
                              tewelvthCell1.innerHTML = "<input   type='hidden'  value='' />";
                          }
                          var mainDiv = document.getElementById("datagrid_container");
                          mainDiv.appendChild(custTable);
                          document.getElementById("makePaymentButton").disabled = false;
                          document.getElementById("removeFromAccountButton").disabled = false;
                          document.getElementById("viewHistoryButton").disabled = false;
                          document.getElementById("viewEditPreferencesButtton").disabled = false;
                          document.getElementById("btnDisconnectionRequest").disabled = false;
                          document.getElementById("btnServiceRequestStatus").disabled = false;
                          document.getElementById("btnServiceRequest").disabled = false;
                          document.getElementById("btnNewConnetionRequest").disabled = false;
                          //Commented for Add consumer grid refresh issue
                          //document.getElementById("btnChangeOfNameRequest").disabled = false;

                          var email = document.getElementById("hdnEmailAddress").value;
                          var param = "emailAddress=" + email + "&consumerNumber=" + consumerNumber;
                          var out = {
                              resp: function consumerAdditionEmailNotification(res) {

                                  var JsonObj = eval("(" + res + ")");
                                  if (JsonObj.toString() == "true") {
                                      // alert(consumerEmailNotificationSuccess);
                                  }
                                  if (JsonObj.toString() == "error") {
                                      // alert(consumerEmailNotificationFail);
                                  }
                              }
                          }
                          callAjax("POST", "wss?uiActionName=consumerAdditionEmailNotification&IsAjax=true", param, false, out.resp);
                      }
                      if (table != null) {
                          var rowCount = table.rows.length;
                          var newRow = table.insertRow(-1);
                          var i = 0, allrows = newRow.parentNode.getElementsByTagName('tr');
                          while (allrows[i] != newRow) ++i;
                          while (allrows[i]) {
                              allrows[i].className = i % 2 ? 'tr_odd' : 'tr_even';
                              ++i;
                          }
                          var firstCell = newRow.insertCell(-1);
                          var secondCell = newRow.insertCell(-1);
                          var thirdCell = newRow.insertCell(-1);
                          var forthCell = newRow.insertCell(-1);
                          var fifthCell = newRow.insertCell(-1);
                          var sixthCell = newRow.insertCell(-1);
                          var seventhCell = newRow.insertCell(-1);
                          var eighthCell = newRow.insertCell(-1);
                          var ninthCell = newRow.insertCell(-1);
                          var tenthCell = newRow.insertCell(-1);
                          var eleventhCell = newRow.insertCell(-1);
                          var tewelvthCell = newRow.insertCell(-1);
                          // alert(eAddConsumerSuccesfully);

                          firstCell.innerHTML = "<input  name='radCustList'  id='radCustList' type='radio' onclick='radioCheck(this)'/>";
                          if (JsonObj.billingConsumptionDTO[0].consumerNo != null) {
                              secondCell.innerHTML = JsonObj.billingConsumptionDTO[0].consumerNo;
                          }
                          if (JsonObj.billingConsumptionDTO[0].billingUnit != null) {
                              thirdCell.innerHTML = JsonObj.billingConsumptionDTO[0].billingUnit;
                              thirdCell.className = "right";
                          }
                          if (JsonObj.billingConsumptionDTO[0].billingUnitDesc != null) {
                              forthCell.innerHTML = JsonObj.billingConsumptionDTO[0].billingUnitDesc;
                              forthCell.className = "left";
                          }
                          if (JsonObj.billingConsumptionDTO[0].billMonth != null) {
                              fifthCell.innerHTML = JsonObj.billingConsumptionDTO[0].billMonth;
                              fifthCell.className = "right";
                          }
                          if (JsonObj.billingConsumptionDTO[0].consumptionUnits != null) {
                              sixthCell.innerHTML = JsonObj.billingConsumptionDTO[0].consumptionUnits;
                              sixthCell.className = "right";
                          }
                          if (JsonObj.billingConsumptionDTO[0].billAmount != null) {
                              seventhCell.innerHTML = JsonObj.billingConsumptionDTO[0].billAmount.toFixed(2);
                              seventhCell.className = "right";
                          }
                          if (JsonObj.billingConsumptionDTO[0].dueDate != null) {
                              eighthCell.innerHTML = JsonObj.billingConsumptionDTO[0].dueDate;
                              eighthCell.className = "right";
                          }
                          if (JsonObj.billingConsumptionDTO[0].billToBePaid != null) {
                              ninthCell.innerHTML = "<input   type='hidden'" + " value=" + JsonObj.billingConsumptionDTO[0].billToBePaid + " />";
                          }
                          //Modified By:Rajashree
                          //Date: 19 OCT 16
                          // Reason:  To enable payment button for PD consumers and display HTML & PDF bill for PD consumers (Consumer Status = 9).

                          //Modified By:Rajashree
                          //Date: 19 APR 17 Release 2.3
                          // Reason:  To disable payment button and hide HTML & PDF bill for Paid Pending consumers.  (Consumer Status = 8).
                          if (JsonObj.billingConsumptionDTO[0].billMonth != null) {

                             // if (JsonObj.billingConsumptionDTO[0].consumerStatus == "9") {
                              if (JsonObj.billingConsumptionDTO[0].consumerStatus == "8") {
                                  tenthCell.innerHTML = "<img style='cursor: pointer; cursor: hand;display:none;' title='View HTML' src='images/ieicon.png'  id='Img1'   width='16' height='16'  onclick='popUpPrintBillHTML(this);'/>";
                                  tenthCell.className = "center";
                              }
                              else {
                                  tenthCell.innerHTML = "<img style='cursor: pointer; cursor: hand;' title='View HTML' src='images/ieicon.png'  id='Img1'   width='16' height='16'  onclick='popUpPrintBillHTML(this);'/>";
                                  tenthCell.className = "center";
                              }
                          }

                          eleventhCell.innerHTML = "<input   type='hidden'" + " value='" + JsonObj.billingConsumptionDTO[0].SDToBePaid + "' />";
                          //added for Checking PD consumers If Consume status= P.D then Y else N
                          if (JsonObj.billingConsumptionDTO[0].consumerStatus == "9") {
                              tewelvthCell.innerHTML = "<input   type='hidden'" + " value='9' />";
                          }
                          else if (JsonObj.billingConsumptionDTO[0].consumerStatus == "8") {
                              tewelvthCell.innerHTML = "<input   type='hidden'" + " value='8' />";
                          }
                          else {
                              tewelvthCell.innerHTML = "<input   type='hidden' value='' />";
                          }

                          document.getElementById("grdCustList").style.visibility = 'visible';
                          document.getElementById("makePaymentButton").disabled = false;
                          document.getElementById("removeFromAccountButton").disabled = false;
                          document.getElementById("viewHistoryButton").disabled = false;
                          document.getElementById("viewEditPreferencesButtton").disabled = false;
                          document.getElementById("btnDisconnectionRequest").disabled = false;
                          document.getElementById("btnServiceRequestStatus").disabled = false;
                          document.getElementById("btnServiceRequest").disabled = false;
                          document.getElementById("btnNewConnetionRequest").disabled = false;
                          //Commented for Add consumer grid refresh issue
                          //document.getElementById("btnChangeOfNameRequest").disabled = false;

                          //for sending an email to consumer account
                          var email = document.getElementById("hdnEmailAddress").value;
                          var param = "emailAddress=" + email + "&consumerNumber=" + consumerNumber;
                          var out = {
                              resp: function consumerAdditionEmailNotification(res) {

                                  var JsonObj = eval("(" + res + ")");
                                  if (JsonObj.toString() == "true") {
                                      // alert(consumerEmailNotificationSuccess);
                                  }
                                  if (JsonObj.toString() == "error") {
                                      // alert(consumerEmailNotificationFail);
                                  }
                              }
                          }
                          callAjax("POST", "wss?uiActionName=consumerAdditionEmailNotification&IsAjax=true", param, false, out.resp);
                          //table not null ends here
                      }
                      //success msg ends here
                  }
                  if (JsonObj.toString().substring(0, 5) == "error") {
                      // alert(JsonObj.toString());
                  }
                  //resp ends here
              }
              //out function ends here
          }
  	    callAjax("POST", "wss?uiActionName=postMyAccount&IsAjax=true", params, false, out.resp);
      }
  	else
  	{
  		if(sessionTimeout == false)
  	    {
  		    displayErrors();
  		}
  		else
  		{
  		       displayErrors();
  		       document.getElementById("uiActionName").value="getCustAccountLogin";
                 document.forms[0].submit();
  		}
  	}
  return false;
}

  function addToErrorList(s) {
    errorList += s + "\n";
  }

  function callAjax(method_type, url, params, varAsync, callBack) {
    //alert("Called callAjaxFunction");
      var xmlHttp;
       if (window.XMLHttpRequest)
       {// code for IE7+, Firefox, Chrome, Opera, Safari
          xmlHttp=new XMLHttpRequest();
        }
       else
       {// code for IE6, IE5
         xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
      }

      xmlHttp.open(method_type,url, varAsync);
      xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      xmlHttp.send(params);

      if (!varAsync) {
          callBack(xmlHttp.responseText);
      }
      else {
          xmlHttp.onreadystatechange = function() {
              if (xmlHttp.readyState == 4) {
                  if (xmlHttp.status == 200) {
                      callBack(xmlHttp.responseText);
                  }
              }
          }
      }
  }

  return Promise.resolve();
}
