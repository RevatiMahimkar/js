const puppeteer = require('puppeteer');

(async() => {
  const browser = await puppeteer.launch({
    headless: false,
    slowMo: 200
  });
  const page = await browser.newPage();

  // Open page.
  await page.goto('https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin');

  await page.setViewport({
    width: 1920,
    height: 1080
  });
  // Focus input field & Type in query.
  await page.type('#identifierId','@selenite.co');

  await page.click('.RveJvd');

  // Focus input field & Type in query.
  await page.type('#password > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > input:nth-child(1)','');

  await page.click('.CwaK9');

  // Submit the form.

  // Keep the browser open.
  // await browser.close();

})();
