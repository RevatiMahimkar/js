const puppeteer = require('puppeteer');
const addConsumerDscom = require('./object');

// async function addConsumer(consumers)
async function addConsumer(consumers) {
  console.log('Launching Browser');
  const browser = await puppeteer.launch({
    headless: true,
    slowMo: 100,
    timeout : 0
  });
  const page = await browser.newPage();

  console.log('Visiting Page');
  // Open page.
  await page.goto('https://wss.mahadiscom.in/wss/wss?uiActionName=getCustAccountLogin');

  var consumers = [
  {
    connectionType: "LT",
    billingUnit: "4542",
    consumerNumber: "000010319897"

  },
  {
    connectionType: "LT",
    billingUnit: "4542",
    consumerNumber: "000010352436"

  },
  {
    connectionType: "HT",
    billingUnit: "539",
    consumerNumber: "000019001911"

  },
  {
    connectionType: "HT",
    billingUnit: "539",
    consumerNumber: "000019000141"
  },
  {
    connectionType: "HT",
    billingUnit: "539",
    consumerNumber: "000019000150"
  },
  {
    connectionType: "LT",
    billingUnit: "4542",
    consumerNumber: "900060000591"
  }
];
  await page.setViewport({
    width: 1920,
    height: 1080
  });
  console.log('entering username');
  // Focus input field & Type in query.
  await page.type('#loginId','tmc.billwise');

  console.log('entering password');
  // Focus input field & Type in query.
  await page.type('#password','Selenite#1');

  console.log('Clicking on Login Button');
  // Submit the form.
  await page.click('#lblLoginButton');

  console.log('waiting for page navigation');
  await page.waitFor('#consumerType');
  console.log('login successful');
  //
  addConsumerToList(consumers);

  async function addConsumerToList(consumers) {

    if(!consumers.length) {
      return await browser.close();
    }

    let value = consumers.shift();
    console.log("Adding consumer ", value.consumerNumber);
    await page.type('#consumerType', value['connectionType']);
    await page.type('#consumerNumber', value['consumerNumber']);
    await page.type('#' + (value['connectionType'] === 'LT' ? 'BU' : 'ddlCircleCode'), value['billingUnit']);
    // await page.click('#lblAdd1');
    await page.evaluate(addConsumerDscom);
    await page.waitFor(2000);
    addConsumerToList(consumers);
  }
  // consumers.forEach(async (value) => {
  //
  //   // await page.evaluate(addConsumerDscom);
  // });
}

addConsumer();
