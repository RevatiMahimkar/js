'use strict';

const Chalk = require('chalk');
const Boom = require('boom');
const _ = require('lodash');

module.exports = (server, options) => [{
        name: 'tasks.find',
        method: find,
        options: {
            bind: server.waterline.collections.tasks,
            // cache: server.plugins.runtime.cache.tasks.find.cache ? server.plugins.runtime.cache.tasks.find : undefined,
            // generateKey: (opts) => JSON.stringify(opts)
        }
    },
    {
        name: 'tasks.findOne',
        method: findOne,
        options: {
            bind: server.waterline.collections.tasks,
            // cache: server.plugins.runtime.cache.tasks.findOne.cache ? server.plugins.runtime.cache.tasks.findOne : undefined,
            // generateKey: (opts) => JSON.stringify(opts)
        }
    },
    {
        name: 'tasks.create',
        method: create,
        options: {
            bind: server.waterline.collections.tasks
        }
    },
    {
        name: 'tasks.edit',
        method: edit,
        options: {
            bind: server.waterline.collections.tasks
        }
    },
    {
        name: 'tasks.destroy',
        method: destroy,
        options: {
            bind: server.waterline.collections.tasks
        }
    }
];

const find = function find(query, next) {

    this
        .find({ limit: query.page_size, skip: ((query.page_no - 1) * query.page_size) })
        .populate("userId")
        .exec((err, tasks) => {

            if (err) {
                console.error(Chalk.bgRed.white(err));
                return next(Boom.badImplementation());
            }
            return next(null, tasks);
        });
};

const findOne = function findOne(id, next) {

    this
        .findOneById(id)
        .populate("userId")
        .exec((err, task) => {

            if (err) {
                console.error(Chalk.bgRed.white(err));
                return next(Boom.badImplementation());
            }
            if (!task) {
                return next(Boom.notFound('task not found'));
            }
            return next(null, task);
        });
};

const create = function create(data, next) {

    this
        .create(data)
        .exec((err, task) => {

            if (err) {
                console.error(Chalk.bgRed.white(err));
                return next(Boom.badImplementation());
            }
            return next(null, task);
        });
};

const edit = function edit(id, data, next) {

    this
        .update({ id }, data)
        .exec((err, task) => {

            if (err) {
                console.error(Chalk.bgRed.white(err));
                return next(Boom.badImplementation());
            }
            if (!task.length) {
                return next(Boom.notFound('task not found'));
            }
            return next(null, _.head(task));
        });
};

const destroy = function destroy(id, next) {

    this
        .destroy({ id }, (err) => {

            if (err) {
                console.error(Chalk.bgRed.white(err));
                return next(Boom.badImplementation());
            }
            return next();
        });
};