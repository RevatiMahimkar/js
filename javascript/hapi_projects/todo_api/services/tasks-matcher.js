'use strict';

const Async = require('async');
const _ = require('lodash');

module.exports = (server, options) => {
    // assign random userId to tasks who don't have one
    server.seneca.add({ role: 'tasks-matcher', cmd: 'matchtasksWithUsers' }, (message, next) => {

        const tasks = server.waterline.collections.tasks;
        const Users = server.waterline.collections.users;

        Async.series({
            usersIds: (cb) => {

                Users
                    .find({}, { select: ['id'] })
                    .exec((err, users) => {

                        if (err) {
                            return cb(err);
                        }
                        cb(null, _.map(users, 'id'));
                    });
            },

            tasksIds: (cb) => {

                tasks
                    .find({ userId: { $exists: false } }, { select: ['id'] })
                    .exec((err, tasks) => {

                        if (err) {
                            return cb(err);
                        }
                        cb(null, _.map(tasks, 'id'));
                    });
            }
        }, (err, ids) => {

            if (err) {
                return next(err);
            }

            const usersIds = ids.usersIds;
            const tasksIds = ids.tasksIds;

            if (!usersIds.length || !tasksIds.length) {
                return next(null, []);
            }

            let userId;

            Async.reduce(tasksIds, {}, (assignmentCount, id, cb) => {

                userId = _.sample(usersIds);

                tasks
                    .update({ id }, { userId })
                    .exec((err, result) => {

                        if (err) {
                            return cb(err);
                        }
                        assignmentCount[userId] = assignmentCount[userId] + 1 || 1;
                        cb(null, assignmentCount);
                    });
            }, (err, results) => {

                if (err) {
                    return next(err);
                }

                return next(null, results);
            });
        });
    });
};