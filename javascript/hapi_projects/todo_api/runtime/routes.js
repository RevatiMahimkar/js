'use strict';

module.exports = {
    tasks: {
        DEFAULT_PAGE_NO: 1,
        DEFAULT_PAGE_SIZE: 10
            // ALLOWsED_TASKS: /^[a-z][a-z0-9_]$/
    },
    // pets: {
    //     DEFAULT_LIMIT: 10,
    //     DEFAULT_OFFSET: 0,
    //     ALLOWED_TYPES: [
    //         'dog',
    //         'cat'
    //     ]
    // },
    users: {
        DEFAULT_PAGE_NO: 1,
        DEFAULT_PAGE_SIZE: 10,
        VALID_USERNAME: /^[a-z][a-z0-9_]{4,}$/
    }
    // someDomain: {
    //   SOME_PARAM: ...
    //   ...
    // }
};