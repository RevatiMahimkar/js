'use strict';

module.exports = {
    find: (request, reply) => reply(request.pre.tasks),
    create: (request, reply) => reply(request.pre.task).created(),
    findOne: (request, reply) => reply(request.pre.task),
    edit: (request, reply) => reply(request.pre.task),
    destroy: (request, reply) => reply().code(204)
};