'use strict';

module.exports = {
    find: (request, reply) => reply(request.pre.users),
    create: (request, reply) => reply(request.pre.user).created(),
    findOne: (request, reply) => reply(request.pre.user),
    edit: (request, reply) => reply(request.pre.user),
    destroy: (request, reply) => reply().code(204)
};