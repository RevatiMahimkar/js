'use strict';

const Validations = require('../validations/tasks');
const Handlers = require('../handlers/tasks');

module.exports = {
    find: {
        validate: Validations.find,
        pre: [
            { method: 'tasks.find(query)', assign: 'tasks' }
        ],
        handler: Handlers.find,
        tags: ['api']
    },

    create: {
        validate: Validations.create,
        pre: [
            { method: 'tasks.create(payload)', assign: 'task' },
            { method: 'utils.buildResourceLocation(path, pre.task.id)', assign: 'location' }
        ],
        handler: Handlers.create,
        tags: ['api']
    },

    findOne: {
        validate: Validations.findOne,
        pre: [
            { method: 'tasks.findOne(params.id)', assign: 'task' }
        ],
        handler: Handlers.findOne,
        tags: ['api']
    },

    edit: {
        validate: Validations.edit,
        pre: [
            { method: 'tasks.edit(params.id, payload)', assign: 'task' }
        ],
        handler: Handlers.edit,
        tags: ['api']
    },

    destroy: {
        validate: Validations.destroy,
        pre: ['tasks.destroy(params.id)'],
        handler: Handlers.destroy,
        tags: ['api']
    }
};