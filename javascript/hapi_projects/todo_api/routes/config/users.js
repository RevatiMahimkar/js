'use strict';

const Validations = require('../validations/users');
const Handlers = require('../handlers/users');

module.exports = {
    find: {
        validate: Validations.find,
        pre: [
            { method: 'users.find(query)', assign: 'users' }
        ],
        handler: Handlers.find,
        tags: ['api']
    },

    create: {
        validate: Validations.create,
        pre: [
            { method: 'users.create(payload)', assign: 'user' },
            { method: 'utils.buildResourceLocation(path, pre.user.id)', assign: 'location' }
        ],
        handler: Handlers.create,
        tags: ['api']
    },

    findOne: {
        validate: Validations.findOne,
        pre: [
            { method: 'users.findOne(params.id)', assign: 'user' }
        ],
        handler: Handlers.findOne,
        tags: ['api']
    },

    edit: {
        validate: Validations.edit,
        pre: [
            { method: 'users.edit(params.id, payload)', assign: 'user' }
        ],
        handler: Handlers.edit,
        tags: ['api']
    },

    destroy: {
        validate: Validations.destroy,
        pre: ['users.destroy(params.id)'],
        handler: Handlers.destroy,
        tags: ['api']
    }
};