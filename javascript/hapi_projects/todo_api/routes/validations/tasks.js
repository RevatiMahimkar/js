'use strict';

const Joi = require('joi');
const Config = require('../../runtime/routes').tasks;
Joi.objectId = require('joi-objectid')(Joi);
const Boom = require('boom');

module.exports = {
    find: {
        failAction: function(request, reply, source, error) {
            reply(Boom.badData(error.data.message.substring(error.data.message.indexOf('[')).replace('[', '').replace(']', '')));
        },
        query: {
            page_no: Joi.number().integer().min(1).default(Config.DEFAULT_PAGE_NO),
            page_size: Joi.number().integer().min(1).default(Config.DEFAULT_PAGE_SIZE)
        }
    },
    create: {
        failAction: function(request, reply, source, error) {
            reply(Boom.badData(error.data.message.substring(error.data.message.indexOf('[')).replace('[', '').replace(']', '')));
        },
        payload: {
            type: Joi.string().required(),
            name: Joi.string().required(),
            userId: Joi.objectId().required()
        }
    },

    findOne: {
        failAction: function(request, reply, source, error) {
            reply(Boom.badData(error.data.message.substring(error.data.message.indexOf('[')).replace('[', '').replace(']', '')));
        },
        params: {
            id: Joi.objectId().required()
        }
    },

    edit: {
        failAction: function(request, reply, source, error) {
            reply(Boom.badData(error.data.message.substring(error.data.message.indexOf('[')).replace('[', '').replace(']', '')));
        },
        params: {
            id: Joi.objectId().required()
        },
        payload: {
            type: Joi.string().optional(),
            name: Joi.string().optional(),
            userId: Joi.objectId().optional()
        }
    },

    destroy: {
        failAction: function(request, reply, source, error) {
            reply(Boom.badData(error.data.message.substring(error.data.message.indexOf('[')).replace('[', '').replace(']', '')));
        },
        params: {
            id: Joi.objectId().required()
        }
    }
};