'use strict';

const Joi = require('joi');
const Config = require('../../runtime/routes').users;
const Boom = require('boom');
Joi.objectId = require('joi-objectid')(Joi);

module.exports = {
    find: {
        failAction: function(request, reply, source, error) {
            reply(Boom.badData(error.data.message.substring(error.data.message.indexOf('[')).replace('[', '').replace(']', '')));
        },
        query: {
            page_no: Joi.number().integer().min(1).default(Config.DEFAULT_PAGE_NO),
            page_size: Joi.number().integer().min(1).default(Config.DEFAULT_PAGE_SIZE)
        }
    },

    create: {
        failAction: function(request, reply, source, error) {
            reply(Boom.badData(error.data.message.substring(error.data.message.indexOf('[')).replace('[', '').replace(']', '')));
        },
        payload: {
            username: Joi.string().regex(Config.VALID_USERNAME).required()
        }
    },

    findOne: {
        failAction: function(request, reply, source, error) {
            reply(Boom.badData(error.data.message.substring(error.data.message.indexOf('[')).replace('[', '').replace(']', '')));
        },
        params: {
            id: Joi.objectId().required()
        }
    },

    edit: {
        failAction: function(request, reply, source, error) {
            reply(Boom.badData(error.data.message.substring(error.data.message.indexOf('[')).replace('[', '').replace(']', '')));
        },
        params: {
            id: Joi.objectId().required()
        },
        payload: {
            username: Joi.string().regex(Config.VALID_USERNAME).optional()
        }
    },

    destroy: {
        failAction: function(request, reply, source, error) {
            reply(Boom.badData(error.data.message.substring(error.data.message.indexOf('[')).replace('[', '').replace(']', '')));
        },
        params: {
            id: Joi.objectId().required()
        }
    }
};