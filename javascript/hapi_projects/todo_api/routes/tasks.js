'use strict';

const Config = require('./config/tasks');

module.exports = (server, options) => [{
        method: 'GET',
        path: '/tasks',
        config: Config.find
    },
    {
        method: 'POST',
        path: '/tasks',
        config: Config.create
    },
    {
        method: 'GET',
        path: '/tasks/{id}',
        config: Config.findOne
    },
    {
        method: 'PATCH',
        path: '/tasks/{id}',
        config: Config.edit
    },
    {
        method: 'DELETE',
        path: '/tasks/{id}',
        config: Config.destroy
    }
];